## Instructions to use this setup (Container and Pipeline) for yourself
- Fork the project into your own GitLab account by clicking the "Fork" button on the main page and follow the instructions

![Fork](Figures/fork.png)

- Remove the fork dependency. This option is in the settings tab under "General". Open the "Advanced" Menu there and remove the fork relationship.

![Remove Dependency](Figures/delete_fork_relationship.png)

- The forked project is now independent and you can change the "input.sh" file without affecting the original repository.