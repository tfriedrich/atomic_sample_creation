#!/bin/bash

align_z cifs/VO_P21.cif [-201] temp.atsk
# align_z input_file [zone_axis] output_file
atomsk temp.atsk \
-duplicate 10 10 10  \
-cut above 25 X -cut below 0 X  \
-cut above 25 Y -cut below 0 Y  \
-cut above 25 Z -cut below 0 Z  \
VO_P21_201.xyz
rm temp.atsk
